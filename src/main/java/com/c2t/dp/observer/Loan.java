package com.c2t.dp.observer;

import java.util.ArrayList;

class Loan implements Subject {
	Observer newPaper;
	
	public Observer getObserver() {
		return newPaper;
	}


	private String type;
	private float interest;
	private String bank;

	public Loan(String type, float interest, String bank) {
		this.type = type;
		this.interest = interest;
		this.bank = bank;
	}

	public float getInterest() {
		return interest;
	}

	public void setInterest(float interest) {
		this.interest = interest;
		//newPaper.update(interest);
		newPaper.update(interest);
	}

	public String getBank() {
		return this.bank;
	}

	public String getType() {
		return this.type;
	}


	@Override
	public void notifyObservers() {
		newPaper.update(interest);
	}

	@Override
	public void registerObserver(Observer observer1) {
		// TODO Auto-generated method stub
		newPaper = observer1;
		
	}

	@Override
	public void removeObserver(Observer observer) {
		// TODO Auto-generated method stub
		
	}

}
