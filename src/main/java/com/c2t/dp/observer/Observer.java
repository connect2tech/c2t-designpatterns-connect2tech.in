package com.c2t.dp.observer;

interface Observer {
    public void update(float interest);
}


