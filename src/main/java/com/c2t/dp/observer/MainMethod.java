package com.c2t.dp.observer;

public class MainMethod {
	public static void main(String[] args) {
		SubjectBank bank = new SubjectBank();
		
		ObserverTV observerTV = new ObserverTV();
		bank.setTv(observerTV);
		bank.changeInterest(10);
	}
}
