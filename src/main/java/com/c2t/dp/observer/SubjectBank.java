package com.c2t.dp.observer;

public class SubjectBank {
	ObserverTV tv;

	public ObserverTV getTv() {
		return tv;
	}

	public void setTv(ObserverTV tv) {
		this.tv = tv;
	}

	public void changeInterest(int rate){
		tv.observer(rate);
	}

}
