package com.c2t.dp.decorator;

public class Main {
	public static void main(String[] args) {
		Food veg = new VegFood();
		
		Food non_veg = new NonVegFood2(veg);
		non_veg.prepareFood();
		
		
	}
}
