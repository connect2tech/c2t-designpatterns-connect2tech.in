package com.c2t.dp.decorator;

public interface Food {
	public String prepareFood();

	public double foodPrice();
}
