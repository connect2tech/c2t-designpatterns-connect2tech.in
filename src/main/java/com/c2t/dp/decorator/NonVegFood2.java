package com.c2t.dp.decorator;

public class NonVegFood2 implements Food {

	Food veg;
	
	NonVegFood2(Food food){
		veg = food;
	}
	
	public String prepareFood() {

		String curry = veg.prepareFood() + "Chicken Curry";
		return curry;
		
	}

	public double foodPrice() {
		
		double d = veg.foodPrice() + 10.5;
		
		return d;
	}
	
	
	
	
	
	/*public NonVegFood2(Food newFood) {
		super(newFood);
	}

	public String prepareFood() {
		return super.prepareFood() + " With Roasted Chiken and Chiken Curry  ";
	}

	public double foodPrice() {
		return super.foodPrice() + 150.0;
	}*/
}