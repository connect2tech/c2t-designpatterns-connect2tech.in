package com.c2t.dp.proxy;

public interface CommandExecutor {

	public void runCommand(String cmd) throws Exception;
}
