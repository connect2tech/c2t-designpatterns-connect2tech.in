package com.c2t.dp.singleton;

public class MainSingleton {
	public static void main(String[] args) {
		
		MySingleton singleton1 = MySingleton.getSingleton();
		System.out.println(singleton1);
		
		MySingleton singleton2 = MySingleton.getSingleton();
		System.out.println(singleton2);
	}
}
