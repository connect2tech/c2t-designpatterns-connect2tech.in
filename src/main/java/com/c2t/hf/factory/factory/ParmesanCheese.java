package com.c2t.hf.factory.factory;

public class ParmesanCheese implements Cheese {

	public String toString() {
		return "Shredded Parmesan";
	}
}
